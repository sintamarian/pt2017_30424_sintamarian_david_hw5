package homework5;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MonitoredData {
	private Timestamp startTime;
	private Timestamp endTime;
	private String activityLabel;
	
	MonitoredData(Timestamp startTime, Timestamp endTime, String activityLabel){
		this.startTime = startTime;
		this.endTime = endTime;
		this.activityLabel = activityLabel;
	}
	
	public Timestamp getStartTime(){
		return this.startTime;
	}
	public Timestamp getEndTime(){
		return this.endTime;
	}
	public String getActivityLabel(){
		return this.activityLabel;
	}
	public String getStringDate(){
		String day = new SimpleDateFormat("yyyy-MM-dd").format(this.startTime);
		return day;
	}
//	public Integer getStartDay(){
//		return this;
//	}
}
