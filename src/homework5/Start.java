package homework5;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Start {
	


	public static void main(String[] args) throws ParseException {

		Stream<String> stream = null;
		try {
			stream = Files.lines(Paths.get("Activities.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		List<MonitoredData> list = stream.map(mapper->{
			String[] mapp = mapper.split("\t\t");
			String action = mapp[2];
			Timestamp startTime = Timestamp.valueOf(mapp[0]);
			Timestamp endTime = Timestamp.valueOf(mapp[1]);
			MonitoredData data = new MonitoredData(startTime, endTime, action);
			return data;
		}).collect(Collectors.toList());
		
		long distinct = list.stream().map(data->data.getStringDate()).distinct().count();
		System.out.println(distinct);
		
		Map<String, Long> numberOfOccurences = list.stream().collect(Collectors.
				groupingBy(data->data.getActivityLabel(),
				Collectors.counting()));
		PrintWriter writer;
		try {
			writer = new PrintWriter("problem2.txt");
			for(Map.Entry<String, Long> oc : numberOfOccurences.entrySet()){
			    writer.println(oc.getKey() + "  " + oc.getValue());
			}
		    writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		
		Map<String, Map<String, Long>> activityCount = list.stream().collect(
					Collectors.groupingBy(data->data.getStringDate(), 
							Collectors.groupingBy(data->data.getActivityLabel(), Collectors.counting())));
		
		try {
			writer = new PrintWriter("problem3.txt");
			for(Entry<String, Map<String, Long>> ac : activityCount.entrySet()){
			    writer.println(ac.getKey());
			    for(Entry<String, Long> a : ac.getValue().entrySet()){
			    	writer.println("\t"+a.getKey() + "  " + a.getValue());
			    }
			}
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		Map<String, Long> duration = list.stream().collect(Collectors.groupingBy(data->data.getActivityLabel(),
				Collectors.summingLong(date->{
					Long time = (date.getEndTime().getTime() - date.getStartTime().getTime());
					return time;
				})));
		duration = duration.entrySet().stream().filter(data->data.getValue()/3600000 > 10).collect(
				Collectors.toMap(da->da.getKey(), da->da.getValue()/(60000*60))
				);
		try {
			writer = new PrintWriter("problem4.txt");
			for(Map.Entry<String, Long> d : duration.entrySet()){
			    writer.println(d.getKey() + "  " + d.getValue()+ " hours");
			}
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		
//		List<String> durationLess = list.stream().collect(Collectors.groupingBy(classifier))	
//		Map<String, Integer> pb5 = list.stream().collect(Collectors.groupingBy(data->data.getActivityLabel(),
//				))
	}

}
